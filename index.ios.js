import { AppRegistry } from 'react-native';
import { App } from './src/scenes/index.js';

AppRegistry.registerComponent('HRProject', () => App);
