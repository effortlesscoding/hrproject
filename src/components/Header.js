import React from 'react';
import { Text, View } from 'react-native';

const Header = () => {
  const { 
    containerStyle,
    textStyle 
  } = styles;
  return (
    <View style={containerStyle}>
      <Text style={textStyle}>Header</Text>
    </View>
  );
};

const styles = {
  textStyle: {
    fontSize: 20,
    textAlign: 'center'
  },
  containerStyle: {
    backgroundColor: '#ececec',
    paddingTop: 20,
    padding: 1
  }
};

export {
  Header,
};
