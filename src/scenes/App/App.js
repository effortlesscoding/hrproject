import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

class App extends Component {
  render() {
    return <Text>Hello. I am rendering!</Text>;
  }
}

export {
  App
};