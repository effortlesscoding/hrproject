import { AppRegistry } from 'react-native';
import { App } from './src/scenes/index.js';

console.log('scenes', App);

AppRegistry.registerComponent('HRProject', () => App);
AppRegistry.runApplication('HRProject', {
  rootTag: document.getElementById('react-root')
});
